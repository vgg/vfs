#!/usr/bin/env python3

## Copyright (C) 2024 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## NAME
##
##   vfs-project-stats - plot counts of projects and usage of a VFS server
##
## SYNOPSIS
##
##   vfs-project-stats [--rev-directory REV-DIR-PATH] [--no-pickle-projects] [--vps-server]
##
## DESCRIPTION
##
##   This script parses all revisions of all projects in a VFS server
##   and plots how the total number of projects changed over time.  It
##   writes the plots to the current working directory.
##
##   Because this script takes a *very long* time, it will write to
##   disk the internal structure of projects and their times in pickle
##   format.  This is meant to enable manual inspection without having
##   to rescan all the files again.  You can disable it by passing
##   `--no-pickle-projects` but you will regret doing it.
#
##   The reason why this takes a *very long* time is because it parses
##   all revisions of all projects to retrieve their timestamp.  This
##   timestamp was created by the VFS server itself and is the only
##   exactish timestamp.  A possible alternative to get the project
##   file ctime from the filesystem but we probably made a copy of the
##   files at some point which broke that.  We could probably
##   "correct" the file ctime though.
##
##   Historically, we also had a VPS server which was basically the
##   same thing as VFS.  If you use this script to get stats of a VPS
##   server, pass the `--vps-server` flag.


import argparse
import datetime
import itertools
import json
import os
import os.path
import pickle
import re
import sys
from typing import Dict, List

import matplotlib.pyplot as plt


def parse_shared_rev_timestamp(timestamp_str: str) -> datetime.datetime:
    return datetime.datetime.utcfromtimestamp(int(timestamp_str) / 1000.0)


def find_shared_rev_timestamp(json_obj, is_vps_server) -> datetime.datetime:
    if is_vps_server:
        timestamp_key = "rev_timestamp"
    else:
        timestamp_key = "shared_rev_timestamp"
    ## VFS replaces a magic string with the timestamp.  In theory,
    ## that magic string should be in .project.shared_rev_timestamp
    ## but VFS doesn't actually care about it and can be anywhere
    ## (which will lead to invalid projects on the application side of
    ## things).  We have two projects
    ## (f8914a4f-a7bc-4cba-84c6-fae05b55ca3d and
    ## 84830868-59d2-4cd6-9d31-a0bfb7b58067) that placed the magic
    ## string at .shared_rev_timestamp so check it if the correct
    ## works (.project.shared_rev_timestamp does exist but it's some
    ## other time in a human readable format).
    try:
        rev_timestamp = json_obj["project"][timestamp_key]
        return parse_shared_rev_timestamp(rev_timestamp)
    except:
        pass
    try:
        rev_timestamp = json_obj[timestamp_key]
        return parse_shared_rev_timestamp(rev_timestamp)
    except:
        pass
    raise Exception("failed to find the shared_rev_timestamp")


def scan_rev_timestamps(
        rev_path: str, is_vps_server: bool
) -> Dict[str, List[datetime.datetime]]:
    lv1_match = re.compile("^[0-9a-f]{2}$")
    lv2_match = re.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")

    # Map project uuid to list of dates (creation date of each
    # revision)
    projects = {}

    for lv1_dir in os.scandir(rev_path):
        if not lv1_dir.is_dir():
            raise Exception(f"'{lv1_dir.name}' not a directory")
        elif not re.fullmatch(lv1_match, lv1_dir.name):
            raise Exception(f"dirname '{lv1_dir.name}' does not match")

        lv1_path = os.path.join(rev_path, lv1_dir.name)
        for lv2_dir in os.scandir(lv1_path):
            if not lv2_dir.is_dir():
                raise Exception(f"'{lv2_dir.name}' not a directory")
            elif not lv2_dir.name.startswith(lv1_dir.name):
                raise Exception(f"{lv2_dir.name} does not start with {lv1_dir.name}")
            elif not re.fullmatch(lv2_match, lv2_dir.name):
                raise Exception(f"dirname '{lv1_dir.name}' does not match")

            project_uuid = lv2_dir.name
            lv2_path = os.path.join(lv1_path, lv2_dir.name)
            rev_dir_entries = sorted(
                os.scandir(lv2_path),
                # We could get the ctime but what if someone has been
                # editing the projects?
                key=lambda n: int(re.fullmatch("rev-(\d+).json", n.name)[1])
            )
            assert project_uuid not in projects
            projects[project_uuid] = []
            for i, rev_dir_entry in enumerate(rev_dir_entries):
                ## This returns weird results, maybe the directories
                ## were copied?
                # projects[project_uuid][i] = rev_dir_entry.stat().st_ctime
                project_fpath = os.path.join(lv2_path, rev_dir_entry.name)
                try:
                    with open(project_fpath, "rb") as fh:
                        json_obj = json.load(fh)
                    projects[project_uuid].append(find_shared_rev_timestamp(json_obj, is_vps_server))
                except Exception as ex:
                    print(f"failed to load '{project_fpath}': {ex}", file=sys.stderr)
                    continue

            # Prevent projects with empty values (for cases of
            # corrupted or invalid JSON files)
            if len(projects[project_uuid]) == 0:
                projects.pop(project_uuid)

    return projects


def plot_cumulative_projects(ax, projects):
    ctimes = sorted([x[0] for x in projects.values()])
    ax.plot(ctimes, range(len(ctimes)))
    ax.set_title("Cumulative number of projects")
    ax.set_ylabel("# Projects")


def plot_active_number_of_projects(ax, projects):
    amonth = datetime.timedelta(days=31)
    c_events = [(x[0], +1) for x in projects.values()]
    f_events = [(x[-1] + amonth, -1) for x in projects.values()]
    events = sorted(c_events + f_events, key=lambda x: x[0])
    counts = list(itertools.accumulate(
        events,
        func=lambda bal, pmt: bal + pmt[1],
        initial=0,
    ))[1:]

    now = datetime.datetime.now()
    idx = next(filter(lambda x: x[1][0] > now, enumerate(events)))[0] -1

    ax.plot([x[0] for x in events[:idx]], counts[:idx])
    ax.set_title("Number of active projects (changed in the last month)")
    ax.set_ylabel("# Projects")


def plot_counts(projects) -> None:
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1)
    fig.set_figheight(fig.get_figheight() *2)
    fig.autofmt_xdate()

    plot_cumulative_projects(ax1, projects)
    plot_active_number_of_projects(ax2, projects)
    xlims = [
        min(ax1.get_xlim()[0], ax2.get_xlim()[0]),
        max(ax1.get_xlim()[1], ax2.get_xlim()[1]),
    ]
    ax1.set_xlim(xlims)
    ax2.set_xlim(xlims)


def main(argv: List[str]) -> int:
    argv_parser = argparse.ArgumentParser()
    argv_parser.add_argument(
        "--rev-directory",
        default="rev",
        help="path to the VFS data `rev' directory",
    )
    argv_parser.add_argument(
        "--vps-server",
        action='store_true',
        help="Whether this is an old VPS server",
    )
    argv_parser.add_argument(
        "--no-pickle-projects",
        dest="pickle_projects",
        action='store_false',
        help="Whether to pickle to disk the internal timestamp representation",
    )
    args = argv_parser.parse_args(argv[1:])


    projects = scan_rev_timestamps(args.rev_directory, args.vps_server)
    if args.pickle_projects:
        with open("project-timestamps.pkl", "wb") as fh:
            pickle.dump(projects, fh)


    plot_counts(projects)
    plt.suptitle("All projects")
    plt.savefig("vfs-project-counts-all.jpg")

    ## The same but only projects with more than 1 revision (at least
    ## one 1 change)
    projects_p1 = {k:v for k,v in projects.items() if len(v) > 1}
    plot_counts(projects_p1)
    plt.suptitle("All projects with more than 1 revision")
    plt.savefig("vfs-project-counts-more-than-1-rev.jpg")

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
