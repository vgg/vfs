#!/bin/sh

# For VFS server, create all the required folders

set -o errexit
set -o nounset

main() {
    echo "Setting up VFS directories layout in $1"
    if [ -e "$1" ]; then
        echo "error: file '$1' already exists" >& 2
        exit 1;
    fi

    mkdir -p "$1"  # make parent directories as required
    mkdir "$1/latest"
    mkdir "$1/rev"
    mkdir "$1/revdb"
    mkdir "$1/log"

    for SUBDIR in latest rev; do
        for i in $(seq 0 255); do
            BUCKET_DIR=$(printf "%.2x\n" $i)
            mkdir "$1/$SUBDIR/$BUCKET_DIR"
        done
    done
}

main "$@"
